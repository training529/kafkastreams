# Probar en local
## Comandos iniciales

```
cd /vagrant
docker compose up -d
export KAFKA_PRODUCER_EVENT_PREFIX="docker run --network=vagrant_default -v /vagrant:/vagrant -it --rm edenhill/kafkacat:1.6.0 -b kafka:29092 -P -t input /vagrant/events/"
```

## Parar kafka
```
docker compose rm --stop --force
```

## Events
```
eval "$KAFKA_PRODUCER_EVENT_PREFIX"event1
eval "$KAFKA_PRODUCER_EVENT_PREFIX"event2
```
